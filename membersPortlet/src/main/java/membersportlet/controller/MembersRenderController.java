package membersportlet.controller;

import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.webcache.WebCacheItem;
import com.liferay.portal.kernel.webcache.WebCachePoolUtil;
import com.sun.tools.sjavac.Log;
import membersportlet.cache.MembersCacheItem;
import membersportlet.dto.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.util.ArrayList;

/**
 * @author Matěj Kulich
 */
@Controller
@RequestMapping("VIEW")
public class MembersRenderController {
	private final WebCacheItem webCacheItem = new MembersCacheItem();

	@RenderMapping
	public ModelAndView prepareMembersView(RenderRequest renderRequest, RenderResponse response, Model model){
		ArrayList<Person> members = new ArrayList<>();
		try {
			members = (ArrayList<Person>) WebCachePoolUtil.get(PortalUtil.getLocale(renderRequest).toString().substring(0,2),webCacheItem);
		} catch (Exception e) {
			WebCachePoolUtil.remove("key");
			Log.error("[MembersPortlet]: Couldnt fetch data. Exception:" + e.getMessage());
		}
		ModelAndView modelAndView= new ModelAndView("members");
		modelAndView.addObject("members", members);
		return modelAndView;
	}
}