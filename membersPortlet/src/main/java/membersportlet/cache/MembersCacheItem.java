package membersportlet.cache;

import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.kernel.webcache.WebCacheItem;
import com.sun.tools.sjavac.Log;
import membersportlet.MembersSparqlQueryConstants;
import membersportlet.dto.Person;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Matěj Kulich
 */
public class MembersCacheItem implements WebCacheItem {

    @Override
    public Object convert(String key) {
        System.out.println("[Members]: Saving members to cache.");
        if(key.equals("en")){
            return getMembers("en");
        }else if(key.equals("cs")){
            return getMembers("cs");
        }else return new ArrayList<>();
    }

    private Object getMembers(String language){
        String queryString = MembersSparqlQueryConstants.GetMembers(language);
        HashMap<String, Person> members = new HashMap<>();
        String url,repository;
        try {
            url = PropsUtil.get("graphdb.url");
            repository = PropsUtil.get("graphdb.repository");
        }catch (Exception e){
            Log.error("[MembersPortlet]: Cannot read properties.");
            return new ArrayList<>();
        }

        try{
            RemoteRepositoryManager manager = new RemoteRepositoryManager(url);
            RepositoryConnection connection = manager.getRepository(repository).getConnection();
            GraphQuery graphQuery = connection.prepareGraphQuery(queryString);
            try (GraphQueryResult result = graphQuery.evaluate()) {
                for (Statement st : result) {
                    String subject = st.getSubject().stringValue();
                    String predicate = st.getPredicate().stringValue();
                    String object = st.getObject().stringValue();
                    if (!members.containsKey(subject)) {
                        members.put(subject, new Person());
                    }
                    if (predicate.endsWith("description")) {
                        members.get(subject).setDescription(object);
                    } else if (predicate.endsWith("firstName")) {
                        members.get(subject).setFirstName(object);
                    } else if (predicate.endsWith("lastName")) {
                        members.get(subject).setLastName(object);
                    } else if (predicate.endsWith("img")) {
                        members.get(subject).setImgLink(object);
                    } else if (predicate.endsWith("workInfoHomepage")) {
                        members.get(subject).setHomepage(object);
                    } else {
                        Log.error("[MembersPortlet] ERROR - while parsing rdf/xml data from repository." ) ;
                        return new ArrayList<>();
                    }
                }
            }
        }catch (RDF4JException e){
            Log.error("[MembersPortlet] Exception " +e.getMessage()) ;
        }
        return new ArrayList<>(members.values());
    }
    @Override
    public long getRefreshTime() {
        return Time.YEAR;
    }
}
