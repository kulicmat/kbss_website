package membersportlet;

public class MembersSparqlQueryConstants {
    public static String GetMembers(String language){
       return "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\n" +
            "PREFIX p:<http://purl.org/dc/terms/>\n" +
            "\n" +
            "construct{\n" +
            "    ?person foaf:firstName ?firstname.\n" +
            "    ?person foaf:lastName ?lastname.    \n" +
            "    ?person p:description ?description.\n" +
            "    ?person foaf:img ?img.\n" +
            "    ?person foaf:workInfoHomepage ?homepage\n" +
            "}WHERE{    \n" +
            "    ?person foaf:firstName ?firstname.\n" +
            "    ?person a foaf:Person.\n" +
            "    ?person foaf:lastName ?lastname.   \n" +
            "    ?person p:description ?desc.\n" +
            "    ?person foaf:img ?img.\n" +
            "    ?person foaf:workInfoHomepage ?homepage.\n" +
            "    ?person foaf:member <http://onto.fel.cvut.cz/ontologies/kbss#KBSS>\n" +
            "\t OPTIONAL {\n" +
            "        ?person p:description ?lang_d .\n" +
            "        FILTER (LANGMATCHES(LANG(?lang_d), \" "+ language +"\"))\n" +
            "    } BIND(COALESCE(?lang_d, ?desc) as ?description)\n" +
            "}\n";
    }
}
