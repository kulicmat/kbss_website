# Nasazení na docker 

1. Stažení image: _docker pull liferay/portal_
1. Pustit image a nastavit Javu 11: _docker run --env JAVA_HOME=/usr/lib/jvm/zulu11 --env JAVA_VERSION=zulu11 -it -p <PORT>:<PORT   > liferay/portal:{tag}_(Přes Dockerfile mi to nešlo)
1. Otevřít admin terminal: _docker exec -u 0 -it <container_name> bash_
1. Nainstalovat nano/vim: apk add nano
1. Přidat:
    * Vytvořit soubor v /opt/liferay/portal-ext.properties -> dopsat konfig (bude v emailu)
    * Po otevření webových stránek se přihlásit pod údaji v emailu.
    * Po přihlášení kliknout na **Menu**(kostka vpravo nahoře)->Control Panel->Instance settings->Localization - v Current ponechat pouze English a Czech
    * **Menu**(kostka vpravo nahoře)->Control Panel->->Instance settings-> Instance Configuration - změnit logo, v General změnit základní údaje a 
        * Home URL: /web/kbss/home
        * Default Logout Page: /web/kbss/home
    * Dále přejít zpět na hlavní stránku, otevřít si Menu vlevo->Configuration->Site settings a změnit Friendly URL na /kbss
1. Zkopírovat soubory ze složky wars na gitu a nahrát je do /opt/liferay/osgi/war 
    * _docker cp membersPortlet.war <container_name>:/opt/liferay/osgi/war_ ->automatický deploy
1. Po stáhnutí settings.lar z gitu přejít na nastavení webové stránky **Menu** vlevo->Publishing-> Import a uploadnout tam settings.lar.
1. (Pokud nebude vidět obrázek- velmi pravděpodobné) Přejít na Hlavní stránku (samozřejmě být přihlášen jako admin), kliknout na **Edit** (tužka nahoře v liště),v pravo v panelu si kliknout na Selection, proklikat se na image-square a vybrat znovu ten obrázek z nabídky a dát Publish.