package filters;

public class SparqlQueryConstants {
    public static String PROJECTS= "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\n" +
            "construct{\n" +
            "    ?s ?p ?o.\n" +
            "} where { \n" +
            "\t?s ?p ?o .\n" +
            "    ?s a foaf:Project\n" +
            "}\n";

    public static String PEOPLE = "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\n" +
            "construct{\n" +
            "    ?s ?p ?o.\n" +
            "} where { \n" +
            "\t?s ?p ?o .\n" +
            "    ?s a foaf:Person\n" +
            "}\n";
}
