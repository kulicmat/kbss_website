package filters;

import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.sun.tools.sjavac.Log;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class SemanticFilter implements Filter {

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(
            ServletRequest servletRequest, ServletResponse servletResponse,
            FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String uri = (String)servletRequest.getAttribute(WebKeys.INVOKER_FILTER_URI);

        Map<String,RDFFormat> formats = new HashMap<String, RDFFormat>(){{
            put("text/turtle",          RDFFormat.TURTLE);
            put("application/rdf+xml",  RDFFormat.RDFXML);
            put("application/n-triples",RDFFormat.NTRIPLES);
            put("application/ld+json",  RDFFormat.JSONLD);
        }};
        if(httpServletRequest.getHeader("accept")!= null && formats.containsKey(httpServletRequest.getHeader("accept"))
                && (uri.equals("/web/kbss/projects") ||  uri.equals("/web/kbss/people"))) {

            String queryString;
            if(uri.equals("/web/kbss/projects")){
                queryString = SparqlQueryConstants.PROJECTS;
            }else {
                queryString = SparqlQueryConstants.PEOPLE;
            }
            String url,repository;
            try {
                url = PropsUtil.get("graphdb.url");
                repository = PropsUtil.get("graphdb.repository");
            }catch (Exception e){
                Log.error("[MyFilter]: Cannot read properties.");
                return;
            }
            try {
                RemoteRepositoryManager manager = new RemoteRepositoryManager(url);
                RepositoryConnection connection = manager.getRepository(repository).getConnection();
                servletResponse.setCharacterEncoding("UTF-8");
                servletResponse.setContentType(httpServletRequest.getHeader("accept"));
                PrintWriter printWriter = servletResponse.getWriter();
                RDFWriter writer = Rio.createWriter(formats.get(httpServletRequest.getHeader("accept")), printWriter);
                connection.prepareGraphQuery(queryString).evaluate(writer);
            }catch(RDF4JException e){
                Log.error("[SemanticFilter]: Exception " +e.getMessage()) ;
            }
        }else{
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}