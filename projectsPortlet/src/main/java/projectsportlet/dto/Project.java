package projectsportlet.dto;


import com.sun.tools.javac.util.List;

import java.util.ArrayList;
import java.util.HashMap;

public class Project {
    private String id = "";
    private String name = "";
    private HashMap<String, ArrayList<String>> partners;
    private String description = "";
    private String subtitle = "";
    private String homepage = "";
    private String acronym = "";

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public Project(){
        partners = new HashMap<>();
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void addPartner(String key, String projectName,String projectHomepage){
        partners.put(key, new ArrayList<>(List.of(projectName,projectHomepage)));
    }
    public void replacePartner(String key, String projectName,String projectHomepage){
        partners.replace(key,new ArrayList<>(List.of(projectName,projectHomepage)));
    }

    public String getName() {
        return name;
    }

    public HashMap<String,ArrayList<String>> getPartners() {
        return partners;
    }

    public String getDescription() {
        return description;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
