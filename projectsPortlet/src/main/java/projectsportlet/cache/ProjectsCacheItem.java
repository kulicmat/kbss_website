package projectsportlet.cache;

import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.kernel.webcache.WebCacheItem;
import com.sun.tools.javac.util.List;
import com.sun.tools.sjavac.Log;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import projectsportlet.ProjectsSparqlQueryConstants;
import projectsportlet.dto.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Matěj Kulich
 */
public class ProjectsCacheItem implements WebCacheItem {

    @Override
    public Object convert(String key) {
        if(key.equals("projects_en")){
            System.out.println("[Projects:] Saving projects to cache.@en");
            return getProjects("en");
        }else if (key.equals("projects_cs")){
            System.out.println("[Projects:] Saving projects to cache.@cs");
            return getProjects("cs");
        }else {
            Log.error("[ProjectsPortlet]: Wrong cache input key.");
            return null;
        }
    }

    @Override
    public long getRefreshTime() {
        return Time.YEAR;
    }

    private Object getProjects(String language){
        String queryString = ProjectsSparqlQueryConstants.getProjects(language);

        HashMap<String, Project> projects = new HashMap<>();
        HashMap<String,String> names = new HashMap<>();
        HashMap<String,String> homepages = new HashMap<>();

        GraphQuery graphQuery = prepareGraphQuery(queryString);
        assert graphQuery != null;
        try (GraphQueryResult result = graphQuery.evaluate()) {
            while (result.hasNext()) {
                Statement st = result.next();
                String subject = st.getSubject().stringValue();
                String predicate = st.getPredicate().stringValue();
                String object = st.getObject().stringValue();
                if(predicate.endsWith("has-participant")){
                    if(!projects.containsKey(subject)){
                        Project tmp = new Project();
                        tmp.addPartner(object,"","");
                        projects.put(subject,tmp);
                    }else{
                        projects.get(subject).addPartner(object,"","");
                    }
                }else if(predicate.endsWith("description")){
                    if(projects.containsKey(subject)){
                        projects.get(subject).setDescription(object);
                    }else{
                        Project tmp = new Project();
                        tmp.setDescription(object);
                        projects.put(subject,tmp);
                    }
                }else if(predicate.endsWith("name")) {
                    names.put(subject, object);
                }else if(predicate.endsWith("homepage")){
                    homepages.put(subject,object);
                }else if (predicate.endsWith("acronym")){
                    if(projects.containsKey(subject)){
                        projects.get(subject).setAcronym(object);
                    }else{
                        Project tmp = new Project();
                        tmp.setAcronym(object);
                        projects.put(subject,tmp);
                    }
                }else{
                    Log.error("[ProjectsPortlet]: Error while parsing rdf/xml output.");
                    return new ArrayList<>();
                }
            }
        }
        for (Map.Entry<String,Project> entry: projects.entrySet()){
            entry.getValue().setName(names.get(entry.getKey()));
            if(homepages.containsKey(entry.getKey())){
                entry.getValue().setHomepage(homepages.get(entry.getKey()));
            }

            for (String partnerID: entry.getValue().getPartners().keySet()){
                entry.getValue().getPartners().replace(partnerID,new ArrayList<>(List.of(names.get(partnerID),homepages.get(partnerID))));
            }
        }
        return projects.values();
    }

    private GraphQuery prepareGraphQuery(String queryString) {
        String username,password,url,repository;
        try {
            username = PropsUtil.get("graphdb.username");
            password = PropsUtil.get("graphdb.password");
            url = PropsUtil.get("graphdb.url");
            repository = PropsUtil.get("graphdb.repository");
        }catch (Exception e){
            System.err.println("[ProjectsPortlet]: Cannot read properties.");
            return null;
        }
        try{
            RemoteRepositoryManager manager = new RemoteRepositoryManager(url);
            manager.setUsernameAndPassword(username,password);
            RepositoryConnection connection = manager.getRepository(repository).getConnection();
            return connection.prepareGraphQuery(queryString);
        }catch (RDF4JException e){
            Log.error("[MembersPortlet] Exception " +e.getMessage()) ;
            return null;
        }
    }
}
