package projectsportlet;

public class ProjectsSparqlQueryConstants {
    public static String PartnersQuery = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n" +
            "CONSTRUCT{\n" +
            "    ?participant a ?org;\n" +
            "                 foaf:name ?name\n" +
            "}WHERE {\n" +
            "    ?participant foaf:name ?anyname.\n" +
            "    ?participant a foaf:Organization.\n" +
            "      \n" +
            "    OPTIONAL {\n" +
            "         ?participant foaf:name ?lang_name .\n" +
            "         FILTER (LANGMATCHES(LANG(?lang_name), \""+"en"+"\"))\n" +
            "     }\n" +
            "     BIND(COALESCE(?lang_name, ?anyname) as ?name)\n" +
            "}\n";

    public static String getProjects(String language){
        return "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX p:<http://purl.org/dc/terms/>\n" +
                "PREFIX ufo:<http://onto.fel.cvut.cz/ontologies/ufo/>\t\t\n" +
                "PREFIX ld:<http://www.linkedmodel.org/schema/vaem#>\n" +
                "construct{\n" +
                "    ?Project foaf:name ?name;\n" +
                "    \t\tp:description ?desc;\n" +
                "    \t\tufo:has-participant ?participant;\n" +
                "    \t\tfoaf:homepage ?homepage;\n" +
                "     \t\tld:acronym ?acronym.\n" +
                "    ?participant foaf:name ?pname;\n" +
                "    \t\tfoaf:homepage ?phomepage\n" +
                "}WHERE{\n" +
                "    ?Project a foaf:Project ;\n" +
                "             foaf:name ?anyprojectname;\n" +
                "             ufo:has-participant ?participant;\n" +
                "             ld:acronym ?anyacronym.\n" +
                "    ?participant foaf:name ?anypname;\n" +
                "    \t\t\ta foaf:Organization;\n" +
                "    \t\t\tfoaf:homepage ?phomepage." +
                "    OPTIONAL {?Project p:description ?anydesc}\n" +
                "    OPTIONAL {?Project p:description ?description\n" +
                "        FILTER (LANGMATCHES(LANG(?description), \""+language+"\" ))} BIND(COALESCE(?description, ?anydesc) as ?desc)\n" +
                "\n" +
                "    OPTIONAL {?Project foaf:homepage ?homepage}\n" +
                "    OPTIONAL {\n" +
                "        ?Project ld:acronym ?acronym_lang \n" +
                "        FILTER (LANGMATCHES(LANG(?acronym_lang), \""+language+"\" ))\n" +
                "    }BIND(COALESCE(?acronym_lang, ?anyacronym) as ?acronym)\n" +
                "\n" +
                "    OPTIONAL { \n" +
                "        ?Project foaf:name ?projectName\n" +
                "        FILTER (LANGMATCHES(LANG(?projectName), \""+language+"\"))\n" +
                "    }BIND(COALESCE(?projectName, ?anyprojectname) as ?name) \n" +
                "    \n" +
                "    OPTIONAL { \n" +
                "        ?participant foaf:name ?participantname\n" +
                "        FILTER (LANGMATCHES(LANG(?participantname), \""+language+"\" ))\n" +
                "    }BIND(COALESCE(?participantname, ?anypname) as ?pname) \n" +
                "}";
    }
}
