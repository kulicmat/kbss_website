package projectsportlet.controller;

import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.webcache.WebCacheItem;
import com.liferay.portal.kernel.webcache.WebCachePoolUtil;
import jdk.internal.jline.internal.Log;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import projectsportlet.ProjectsSparqlQueryConstants;
import projectsportlet.cache.ProjectsCacheItem;
import projectsportlet.dto.Project;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

/**
 * @author Matěj Kulich
 */
@Controller
@RequestMapping("VIEW")
public class ProjectsRenderController {

	private final WebCacheItem webCacheItem = new ProjectsCacheItem();

	@RenderMapping
	@SuppressWarnings("unchecked")
	public ModelAndView prepareProjectsMainView(RenderRequest renderRequest, RenderResponse renderResponse){
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		Collection<Project> projects = new ArrayList<>();

		try {
			projects = (Collection<Project>) WebCachePoolUtil.get("projects_"+PortalUtil.getLocale(renderRequest).toString().substring(0,2),webCacheItem);
		} catch (Exception e) {
			WebCachePoolUtil.remove("projects_"+PortalUtil.getLocale(renderRequest).toString().substring(0,2));
			System.err.println("Exceptioon: " + e + " " + Arrays.toString(e.getStackTrace()));
		}
		ModelAndView modelAndView = new ModelAndView("projects");
		modelAndView.addObject("projects", new ArrayList<>(projects));
		return modelAndView;
	}
;
	@SuppressWarnings("unchecked")
	@RenderMapping(params="render=addProject")
	public ModelAndView prepareAddProjectView(){
		String url,repository;
		try {
			url = PropsUtil.get("graphdb.url");
			repository = PropsUtil.get("graphdb.repository");
		}catch (Exception e){
			System.err.println("[projectsPortlet]: Cannot read properties.");
			return null;
		}
		RemoteRepositoryManager manager = new RemoteRepositoryManager(url);
		RepositoryConnection connection = manager.getRepository(repository).getConnection();

		String queryString = ProjectsSparqlQueryConstants.PartnersQuery;

		HashMap <String,String> partners = new HashMap<>();
		GraphQuery graphQuery = connection.prepareGraphQuery(queryString);
		assert graphQuery != null;
		try (GraphQueryResult result = graphQuery.evaluate()) {
			while (result.hasNext()) {
				Statement st = result.next();
				partners.put(st.getSubject().stringValue(),st.getSubject().stringValue().split("#")[1]);
			}
		}
		ModelAndView modelAndView= new ModelAndView("addProject");
		modelAndView.addObject("partners", partners);
		return modelAndView;
	}

	@ActionMapping(params="action=addProject")
	public void handleProjectsAction(ActionRequest request, ActionResponse response){
		String uri = request.getParameter("uri");
		String homepage = request.getParameter("homepage");
		String project_en = request.getParameter("projecten");
		String acronym_en = request.getParameter("acronymen");
		String img = request.getParameter("img");
		String newProjectInfo_en = request.getParameter("newProjectInfoen");
		String project_cz = request.getParameter("projectcz");
		String acronym_cz = request.getParameter("acronymcz");
		String newProjectInfo_cz = request.getParameter("newProjectInfocz");
		String[] partners = request.getParameterValues("partners[]");


//		//TODO: validation for the incoming request
		String username,password,url,repository;
		try {
			username = PropsUtil.get("graphdb.username");
			password = PropsUtil.get("graphdb.password");
			url = PropsUtil.get("graphdb.url");
			repository = PropsUtil.get("graphdb.repository");
		}catch (Exception e){
			Log.error("[ProjectsPortlet]: Cannot read credentials from properties.");
			return;
		}

		RemoteRepositoryManager manager = new RemoteRepositoryManager(url);
		manager.setUsernameAndPassword(username,password);

		StringBuilder queryString = new StringBuilder("PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\n" +
				"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
				"PREFIX p:<http://purl.org/dc/terms/>\n" +
				"PREFIX ufo:<http://onto.fel.cvut.cz/ontologies/ufo/>\n" +
				"PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
				"PREFIX ld:<http://www.linkedmodel.org/schema/vaem#>\n" +
				"INSERT DATA{\n" +
				 "<" +uri +">"+ " a foaf:Project;\n"
					+ "a owl:NamedIndividual; \n" +
				(!homepage.isEmpty() ? ("foaf:homepage \"" + homepage + "\";\n") : "") +
				(!project_en.isEmpty() ? ("foaf:name \"" + project_en + "\"@en;\n") : "") +
				(!project_cz.isEmpty() ? ("foaf:name \"" + project_cz + "\"@cs;\n") : "") +
				(!img.isEmpty() ? ("foaf:img \"" + img + "\";\n") : "") +
				(!acronym_en.isEmpty() ? ("ld:acronym \"" + acronym_en) + "\"@en;\n" : "") +
				(!acronym_cz.isEmpty() ? ("ld:acronym \"" + acronym_cz) + "\"@cs;\n" : "") +
				(!newProjectInfo_cz.isEmpty() ? ("p:description \"" + newProjectInfo_cz + "\"@cs;\n") : "") +
				(!newProjectInfo_en.isEmpty() ? ("p:description \"" + newProjectInfo_en + "\"@en;\n") : ""));

		if (partners != null){
			for (String partner: partners) {
				queryString.append("ufo:has-participant <").append(partner).append(">;\n");
			}
		}
		queryString.append(".}");
		try{
			RepositoryConnection connection = manager.getRepository(repository).getConnection();
			connection.prepareUpdate(queryString.toString()).execute();
			WebCachePoolUtil.clear();
		}
		catch (RDF4JException e) {
			Log.error("[ProjectsPortlet]: " +e);
		}
	}

	@ActionMapping(params="action=clearCache")
	public void handleProjectsAction(){
		WebCachePoolUtil.clear();
	}

}